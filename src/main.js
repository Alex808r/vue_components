import { createApp, defineAsyncComponent } from "vue";
import App from "./App.vue";
import "./theme.css";

import TheHeader from "./components/TheHeader.vue";

const app = createApp(App);

// global registation component
app.component("the-header", TheHeader);

// асинхронный компонент
app.component(
  "app-async",
  defineAsyncComponent(() => {
    return import("./components/AppAsync");
  })
);

app.mount("#app");
